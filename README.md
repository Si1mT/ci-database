# ci-database

 - create inital seed file for database (sql schema or simply seeder)
 - create js module getData.js to get data from database using filters
 - create test with snapshot testing that is getting some data from this module with differnet filters
 - run this test both locally and in ci

# run docker

# run docker services in backgroun
docker-compose up -d --force-recreate

# stop mysql and kill its storage (to load new dump)
docker-compose down -v

# show container logs for mysql
docker-compose logs -f mysql

# connect to mysql shell
docker-compose exec mysql mysql -h mysql -u test -ptest test

# run shell in node container (so you can run there any command you want)
docker-compose run node bash

# run node container (with default command "yarn run test")
docker-compose up node

